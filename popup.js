function updateForm() {
	chrome.extension.sendMessage(
		{command: "GET_CONFIG"},
		function (response) {
			var config = response.config;
			configForm.startButton.hidden = config.running;
			configForm.stopButton.hidden = !config.running;
			configForm.targetUrl.value = config.targetUrl;
			configForm.failureUrl.value = config.failureUrl;
			configForm.domain.value = config.domain;
			configForm.delay.value = config.delay;
			configForm.configJson.value = JSON.stringify(normalizeConfigJson(config));
		}
	);
}

function normalizeConfigJson(config) {
	for (var key in config) {
		if (config.hasOwnProperty(key)) {
			switch(key) {
				case 'targetUrl':
				case 'failureUrl':
				case 'domain':
				case 'delay':
					break;
				default:
					delete config[key];
					break;
			}
		}
	}
	return config;
}

function updateConfig() {
	var config = {
		targetUrl: configForm.targetUrl.value,
		failureUrl: configForm.failureUrl.value,
		domain: configForm.domain.value,
		delay: configForm.delay.value,
	};
	chrome.extension.sendMessage(
		{command: "UPDATE_CONFIG", config: config},
		function (response) {
			updateForm();
		}
	);
}

function loadConfig() {
	var config = normalizeConfigJson(JSON.parse(configForm.configJson.value));
	chrome.extension.sendMessage(
		{command: "UPDATE_CONFIG", config: config},
		function (response) {
			updateForm();
		}
	);
}

function start() {
	chrome.extension.sendMessage(
		{command: "START"},
		function (response) {
			updateForm();
		}
	);
}

function stop() {
	chrome.extension.sendMessage(
		{command: "STOP"},
		function (response) {
			updateForm();
		}
	);
}

function bindEvents() {
	configForm.stopButton.onclick = stop;
	configForm.startButton.onclick = start;
	configForm.updateButton.onclick = updateConfig;
	configForm.loadConfigButton.onclick = loadConfig;
}

function init() {
	bindEvents();
	updateForm();
}

addEventListener("load", init, false);
